import os

from phagetermvirome.IData_handling import genomeFastaRecovery,totReads
from phagetermvirome.main_utils import inputRawDataArgs,functionalParms,InputDerivedDataArgs,technicalParms

TEST_DIR_PATH = os.path.dirname(os.path.abspath(__file__))
TEST_DATA_PATH = os.path.join(TEST_DIR_PATH, "..", "test-data")

class Data4Test:
    def __init__(self,fastq,fasta,paired="",hostseq="",seed=20,edge=500,insert_max=1000,limit_coverage=11,virome=0):
        self.inDRawArgs=inputRawDataArgs(fastq,fasta,None,None,paired,None,nrt=False)
        #self.inDRawArgs.fastq=fastq
        #return genome, name, genome_rejected
        self.refseq_list,name,rej=genomeFastaRecovery(fasta, 500, edge, host_test = 0)
        self.fParms=functionalParms(seed, None, None, limit_coverage, virome, None)
        self.fParms.edge = edge
        self.fParmsinsert_max = insert_max
        self.inDArgs=InputDerivedDataArgs(self.inDRawArgs,self.fParms)
        if hostseq!="": # expect name of fasta file containing hostseq
            hs,name,rej=genomeFastaRecovery(fasta, 500, edge, host_test=0)
            self.inDArgs.hostseq = hs[0]
        else:
            self.inDArgs.hostseq =hostseq

        self.tParms=technicalParms(1,None,250,None,None,None,None,None, \
                     0,None,False,None,None,0,"")
        #self.core=1
        #self.core_id=0,
        #self.limit_coverage=limit_coverage
        #self.virome=virome
        self.line_start=0
        self.line_end=totReads(fastq)

def buildTestData():
    l=[]
    l.append(Data4Test(data_path("COS-5.fastq"), data_path("COS-5.fasta")))
    l.append(Data4Test(data_path("COS-3.fastq"), data_path("COS-3.fasta")))
    l.append(Data4Test(data_path("DTR-short.fastq"), data_path("DTR-short.fasta")))
    l.append(Data4Test(data_path("DTR-long.fastq"), data_path("DTR-long.fasta")))
    l.append(Data4Test(data_path("Headful.fastq"), data_path("Headful.fasta")))
    l.append(Data4Test(data_path("Mu-like_R1.fastq"), data_path("Mu-like.fasta"), data_path("Mu-like_R2.fastq"), data_path("Mu-like_host.fasta")))
    l.append(Data4Test(data_path("Virome.fastq"), data_path("Virome.fasta"), virome=1))
    return l

def data_path(filename):
    '''append the test data path to the filename string.
    '''
    return os.path.join(TEST_DATA_PATH, filename)
