##@file test_functions_PhageTerm_for_multi.py
#
#
# Check that readsCoverage can write its result to a file and that they can be read again to retrieve results.
##@author vlegrand@pasteur.fr

import sys
sys.path.append("..")
sys.path.append("../phagetermvirome")

import unittest
import numpy as np
import os
import shutil

from Data4Test import buildTestData, Data4Test
from phagetermvirome.functions_PhageTerm import readsCoverage
from phagetermvirome.readsCoverage_res import loadRCRes,RCRes,RCCheckpoint_handler
from phagetermvirome.debug_utils import ReadMappingInfoLogger



class Test_functions_Phageterm (unittest.TestCase):
    # Test that readsCoverage results are saved in the appropriate structure and can be read again for later work.
    def testDumpAndReadAgainRes(self):
        l_data = buildTestData()
        chk_handler = RCCheckpoint_handler(0,None,False)
        dir_cov_res = os.path.join(os.getcwd(),"tmp")
        os.mkdir(os.path.join(os.getcwd(),"tmp")) # TODO: when switching to python3, use the tempfile module
        for d in l_data:
            idx_seq = 0
            for refseq in d.refseq_list:
                return_dict = dict()
                d.tParms.dir_cov_mm =None
                readsCoverage(d.inDRawArgs, refseq, d.inDArgs, d.fParms,return_dict, 0,d.line_start, d.line_end, \
                        d.tParms, chk_handler,idx_seq,None)
                r1=RCRes(return_dict[0][0],return_dict[0][1],return_dict[0][2],\
                         return_dict[0][3],return_dict[0][4],return_dict[0][5],\
                         return_dict[0][6],return_dict[0][7],return_dict[0][8],return_dict[0][9])
                r1.save(os.path.join(dir_cov_res,"r1"))
                d.tParms.dir_cov_mm=dir_cov_res
                # readsCoverage(inRawDArgs, refseq, inDArgs, fParms, return_dict, line_start, line_end, tParms, \
                #               idx_refseq=None, logger=None)
                readsCoverage(d.inDRawArgs, refseq,d.inDArgs , d.fParms, None,0, d.line_start, d.line_end, d.tParms,chk_handler,idx_seq,logger=None)
                fname="coverage"+str(idx_seq)+"_0.npz"
                fic_name=os.path.join(dir_cov_res,fname)
                print("checking: ",fic_name)
                self.assertTrue(os.path.exists(fic_name))
                res=loadRCRes(fic_name)
                self.assertEqual(res.termini_coverage.shape,return_dict[0][0].shape) # These dictionnaries cannot have exact same content because of random picking of kmer match positions in case th
                self.assertEqual(res.whole_coverage.shape, return_dict[0][1].shape)
                self.assertEqual(res.paired_whole_coverage.shape, return_dict[0][2].shape)
                self.assertEqual(res.phage_hybrid_coverage.shape, return_dict[0][3].shape)
                self.assertEqual(res.host_hybrid_coverage.shape, return_dict[0][4].shape)
                self.assertEqual(res.host_whole_coverage.shape, return_dict[0][5].shape)
                self.assertTrue(res.list_hybrid.shape, return_dict[0][6].shape)
                self.assertEqual(res.reads_tested, return_dict[0][9])
                r1b=loadRCRes(os.path.join(dir_cov_res,"r1.npz"))
                self.assertTrue(np.array_equal(r1b.termini_coverage,r1.termini_coverage))
                self.assertTrue(np.array_equal(r1b.whole_coverage,r1.whole_coverage))
                self.assertTrue(np.array_equal(r1b.paired_whole_coverage,r1.paired_whole_coverage))
                self.assertTrue(np.array_equal(r1b.phage_hybrid_coverage,r1.phage_hybrid_coverage))
                self.assertTrue(np.array_equal(r1b.host_hybrid_coverage,r1.host_hybrid_coverage))
                self.assertTrue(np.array_equal(r1b.host_whole_coverage,r1.host_whole_coverage))
                self.assertTrue(np.array_equal(r1b.list_hybrid,r1.list_hybrid))
                self.assertTrue(np.array_equal(r1b.insert,r1.insert))
                self.assertTrue(np.array_equal(r1b.paired_mismatch,r1.paired_mismatch))
                self.assertTrue(np.array_equal(r1b.reads_tested,r1.reads_tested))
                idx_seq += 1
        shutil.rmtree(dir_cov_res)
    #
    # # VL: start basic testing of sum_reads_coverage_for_seq for multimachine mode
    def test_basic_sum_readsCoverage_for_seq(self):
        from phagetermvirome.seq_processing import sum_readsCoverage_for_seq
        d=Data4Test("data/G-janv_S2_R1_001.fastq.500","data/seq1_2_3.fasta", paired="data/G-janv_S2_R2_001.fastq.500", \
                    hostseq="", seed=20, edge=500, insert_max=1000, limit_coverage=11, virome=0)
        dir_cov_res=os.path.join(os.getcwd(),"data")
        idx_refseq=0
        nb_pieces=1
        dir_seq_res = os.path.join(os.getcwd(), "seq_res")
        os.mkdir(dir_seq_res)
        DR_path=os.path.join(os.getcwd(), "DR")
        os.mkdir(DR_path)
        sum_readsCoverage_for_seq(dir_cov_res, idx_refseq, nb_pieces, d.inDArgs, d.fParms, d.inDRawArgs, dir_seq_res, DR_path)
        self.assertTrue(os.path.exists("DR/UNKNOWN/NODE_1_length_422445"))
        shutil.rmtree(dir_seq_res)
        shutil.rmtree(DR_path)



    # Checks that checkpoints are created and that their content is correct.
    def test_checkpoint_creation(self):
        # last line is 11150
        d=Data4Test("../test-data/COS-5.fastq", "../test-data/COS-5.fasta")
        dir_chk = os.path.join(os.getcwd(), "tmp2")
        os.mkdir(dir_chk)
        d.tParms.dir_chk=dir_chk
        d.tParms.test_mode=True
        return_dict = dict()
        d.tParms.dir_cov_mm = None
        chk_handler = RCCheckpoint_handler(d.tParms.chk_freq, d.tParms.dir_chk, d.tParms.test_mode)
        readsCoverage(d.inDRawArgs, d.refseq_list[0], d.inDArgs, d.fParms, return_dict, 0, d.line_start, d.line_end, \
                      d.tParms, chk_handler, 0, None)
        fic_name = "chk_0_0_11150_291333.npz"
        full_fname = os.path.join(dir_chk, fic_name)
        self.assertTrue(os.path.exists(full_fname))
        list_f = os.listdir(dir_chk)
        self.assertTrue(len(list_f)==1)
        wr=chk_handler.load(0,0)
        self.assertEqual(wr.read_match,291333)
        self.assertEqual(wr.count_line,11150)
        self.assertEqual(wr.interm_res.host_len,0)
        self.assertEqual(wr.interm_res.gen_len,3012)
        self.assertEqual(int(wr.interm_res.reads_tested),2787) # 2796?
        shutil.rmtree(dir_chk)


    ## Checks thst in production mode, all checkpoints are deleted at the end.
    def test_checkpoint_end(self):
        # last line is 11150
        d=Data4Test("../test-data/COS-5.fastq", "../test-data/COS-5.fasta")
        dir_chk = os.path.join(os.getcwd(), "tmp3")
        os.mkdir(dir_chk)
        d.tParms.chk_freq=1
        d.tParms.dir_chk=dir_chk
        d.tParms.test_mode=False
        return_dict = dict()
        d.tParms.dir_cov_mm = None
        chk_handler = RCCheckpoint_handler(d.tParms.chk_freq, d.tParms.dir_chk, d.tParms.test_mode)
        readsCoverage(d.inDRawArgs, d.refseq_list[0], d.inDArgs, d.fParms, return_dict, 0, d.line_start, d.line_end, \
                      d.tParms, chk_handler, 0, None)
        list_f = os.listdir(dir_chk)
        self.assertTrue(len(list_f)==0)
        shutil.rmtree(dir_chk)

    # ## checks that readsCoverage restarts from ceckpoint and not from the beginning.
    def test_restart_from_checkpoint(self):
        d = Data4Test("../test-data/COS-5.fastq", "../test-data/COS-5.fasta")
        dir_chk = os.path.join(os.getcwd(), "tmp4")
        os.mkdir(dir_chk)
        d.tParms.dir_chk = dir_chk
        d.tParms.chk_freq=1
        d.tParms.test_mode = False
        shutil.copy("data/chk_0_0_38_863.npz",dir_chk)
        return_dict = dict()
        d.tParms.dir_cov_mm = None
        logger=ReadMappingInfoLogger()
        chk_handler = RCCheckpoint_handler(d.tParms.chk_freq, d.tParms.dir_chk, d.tParms.test_mode)
        idx_seq=chk_handler.getIdxSeq(0)
        self.assertEqual(idx_seq,0)
        readsCoverage(d.inDRawArgs, d.refseq_list[0], d.inDArgs, d.fParms, return_dict, 0,d.line_start, d.line_end, \
                      d.tParms, chk_handler,0, logger)
        wr=logger.rw_lst[0]
        self.assertEqual(wr.read_match,863)
        self.assertEqual(wr.count_line,38)
        self.assertEqual(wr.interm_res.host_len,0)
        self.assertEqual(wr.interm_res.gen_len,3012)
        self.assertEqual(int(wr.interm_res.reads_tested),9)
        shutil.rmtree(dir_chk)

    def test_restart_from_checkpoint2(self):
        d=Data4Test("../test-data/Virome.fastq","../test-data/Virome.fasta")
        dir_chk = os.path.join(os.getcwd(), "tmp5")
        os.mkdir(dir_chk)
        d.tParms.dir_chk = dir_chk
        d.tParms.chk_freq = 5
        d.tParms.test_mode = False
        shutil.copy("../test-data/chk_0_2_10_0.npz", dir_chk)
        logger = None
        chk_handler = RCCheckpoint_handler(d.tParms.chk_freq, d.tParms.dir_chk, d.tParms.test_mode)
        idx_seq = chk_handler.getIdxSeq(0)
        self.assertEqual(idx_seq, 2)
        start=idx_seq
        return_dict = dict()
        for seq in d.refseq_list[start:]:
            print("going to process sequence: ",idx_seq)
            print(seq)
            readsCoverage(d.inDRawArgs, seq, d.inDArgs, d.fParms, return_dict, 0, d.line_start, d.line_end, \
                      d.tParms, chk_handler, idx_seq, logger)
            idx_seq+=1
        self.assertEqual(idx_seq,5)
        shutil.rmtree(dir_chk)



if __name__ == "__main__":
    unittest.main()
