#!/bin/sh
## step 1 script
## example of command for using this script:
## sbatch -a 0-119%20 jarray_phageterm_readsCoverage.sh 120
## Argument is total number of cores used (same as number of chunks the reads will be divided into)
cd DIR_WHERE_YOU_CLONED_THE_SOURCES
srun phageterm_readsCoverage_multimachine.sh $1 ${SLURM_ARRAY_TASK_ID}
