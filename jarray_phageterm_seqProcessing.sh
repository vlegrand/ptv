#!/bin/sh
## step 2 script
## This script must not be used before all jobs launched by jarray_phageterm_readsCoverage.sh are terminated.
# In order to run it, you must know how many sequences were processed at step 1. It is in the job's output file.
## sbatch -a 0-586 jarray_phageterm_readsCoverage.sh
cd DIR_WHERE_YOU_CLONED_THE_SOURCES
srun phageterm_seqProcessing_multimachine.sh ${SLURM_ARRAY_TASK_ID}
