#/bin/bash

source common_nrt.sh

echo "running PhageTerm on HK97 genome"
python ../PhageTerm.py -f $DATA_PATH/R1_1M_READS_EACH_PHAGE.fastq.20 -p $DATA_PATH/R2_1M_READS_EACH_PHAGE.fastq.20 -r $DATA_PATH/HK97_assembly.fasta
check_HK97_P1_T4_res
ret_code=$?
if [ "$ret_code" -ne "0" ]; then
    exit $ret_code
fi
echo "checking phage genome in output fasta file "
res=`diff $REF_RES_PATH/res_HK97.20/NA_sequence.fasta NA_sequence.fasta`
ret_code=$?
if [ "$ret_code" -ne "0" ]; then
    echo $res
    exit $ret_code
fi
echo "checking peak values"
# sort -t , -r -k 4 NA_statistics.csv # sorts the csv file by descending order of values in field 4
vars=$(sort -t , -r -k 4 NA_statistics.csv|head -n 2|tail -n 1| cut -d , -f 1,4,6) # gets position, T and p-value for + peak
IFS=','
arrv=($vars)
pos=${arrv[0]}
T=${arr[1]}
pval=${arr[2]}

if [ "$pos" -ne "4383" ]; then
    echo "HK97.20 wrong position for + peak. expected 4383 got: "$pos
    exit 100
fi

if (( $T < 0.1 )); then
    echo "T <0.1 for + peak. Expected a value > 0.1"
    exit 101
fi

if (( $pval>0.05 )); then
    echo "p value >0.05 for + peak. Expected a value <0.05"
    exit 102
fi

vars=$(sort -t , -r -k 10 NA_statistics.csv|head -n 2|tail -n 1| cut -d , -f 1,10,12)
IFS=','
arrv=($vars)
pos=${arrv[0]}
T=${arr[1]}
pval=${arr[2]}

if [ "$pos" -ne "4372" ]; then
    echo "HK97.20 wrong position for + peak. expected 4372 got: "$pos
    exit 103
fi

if (( $T < 0.1 )); then
    echo "T <0.1 for + peak. Expected a value > 0.1"
    exit 104
fi

if (( $pval>0.05 )); then
    echo "p value >0.05 for + peak. Expected a value <0.05"
    exit 104
fi
