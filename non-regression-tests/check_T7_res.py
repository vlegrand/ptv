import csv

from common import check_files_repeat, check_peak, remove_files_repeat, K_OK

DATA_PATH="./data"
REF_RES_PATH="./reference_results/res_T7.20/"
ref_filename=REF_RES_PATH+"NA_sequence.fasta"
seq_filename="NA_sequence.fasta"
stats_filename="NA_statistics.csv"
report_filename="NA_PhageTerm_report.pdf"
repeats_filename="NA_direct-term-repeats.fasta"

ret=check_files_repeat(seq_filename,stats_filename,report_filename,repeats_filename,ref_filename)
if ret !=K_OK:
    exit(ret)


reader = csv.reader(open("NA_statistics.csv"), delimiter=",")
sortedlist = sorted(reader, key=lambda row: row[3], reverse=True)
r=sortedlist[1]

pos_peak=r[0]
T=r[3]
pval=r[5]
# 7740,0.48,3.73e-215
ret=check_peak(7740,pos_peak,T,pval)
if ret!=K_OK:
    exit(ret)

reader = csv.reader(open("NA_statistics.csv"), delimiter=",")
sortedlist = sorted(reader, key=lambda row: row[9], reverse=True)
r=sortedlist[1]

pos_peak=r[0]
T=r[9]
pval=r[11]
# 7899,0.56,1.93e-252
ret=check_peak(7899,pos_peak,T,pval)
if ret!=K_OK:
    exit(ret)

remove_files_repeat(seq_filename,stats_filename,report_filename,repeats_filename)
