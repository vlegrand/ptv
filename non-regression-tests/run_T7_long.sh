#/bin/bash
DATA_PATH=./data
SCRIPT_PATH=../phagetermvirome

echo "running PhageTerm on T7 genome and a dataset with a minimum 50x coverage"
python $SCRIPT_PATH/PhageTerm.py -f "$DATA_PATH/R1_1M_READS_EACH_PHAGE(1).fastq.4" -p "$DATA_PATH/R2_1M_READS_EACH_PHAGE(1).fastq.4" -r $DATA_PATH/T7_assembly.fasta --nrt || exit 1
python check_T7_class.py || exit 2
