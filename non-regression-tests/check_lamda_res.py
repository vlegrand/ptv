import csv


from common import check_files, check_peak, remove_files, K_OK

DATA_PATH="./data/"
REF_RES_PATH="./reference_results/res_lamda.20/"
ref_filename=REF_RES_PATH+"NA_sequence.fasta"
new_filename="NA_sequence.fasta"
stats_filename="NA_statistics.csv"
report_filename="NA_PhageTerm_report.pdf"

# 1rst, check that all files exist
ret=check_files(new_filename,stats_filename,report_filename,ref_filename)
if ret !=K_OK:
    exit(ret)


reader = csv.reader(open("NA_statistics.csv"), delimiter=",")
sortedlist = sorted(reader, key=lambda row: row[3], reverse=True)
r=sortedlist[1]
pos_peak=r[0]
T=r[3]
pval=r[5]

ret=check_peak(10868,pos_peak,T,pval)
if ret!=K_OK:
    exit(ret)


reader = csv.reader(open("NA_statistics.csv"), delimiter=",")
sortedlist = sorted(reader, key=lambda row: row[9], reverse=True)
r=sortedlist[1]

pos_peak=r[0]
T=r[9]
pval=r[11]

ret=check_peak(10879,pos_peak,T,pval)
if ret!=K_OK:
    exit(ret)


# clean files
remove_files(new_filename,stats_filename,report_filename)

