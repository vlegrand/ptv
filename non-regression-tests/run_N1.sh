#/bin/bash
DATA_PATH=./data
REF_RES_PATH=./reference_results
SCRIPT_PATH=../phagetermvirome

echo "running PhageTerm on StaphN1 genome"
python $SCRIPT_PATH/PhageTerm.py -f $DATA_PATH/R1_1M_READS_EACH_PHAGE.fastq.20 -p $DATA_PATH/R2_1M_READS_EACH_PHAGE.fastq.20 -r $DATA_PATH/Staph1N_assembly.fasta ||exit 1
python check_N1_res.py ||exit 1
