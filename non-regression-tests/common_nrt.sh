#/bin/bash

DATA_PATH=./data
REF_RES_PATH=./reference_results

check_HK97_P1_T4_res()
{
if [ ! -f NA_PhageTerm_report.pdf ]; then
    echo "NA_PhageTerm_report.pdf does not exist!"
    exit 5
fi
if [ ! -f NA_sequence.fasta ]; then
    echo "NA_sequence.fasta does not exist!"
    exit 6
fi
if [ ! -f NA_statistics.csv ]; then
    echo "NA_statistics.csv does not exist!"
    exit 7
fi
}

copy_HK97_P1_T4_res()
{
    mkdir $1
    mv NA_PhageTerm_report.pdf $1/NA_PhageTerm_report.pdf
    mv NA_sequence.fasta $1/NA_sequence.fasta
    mv NA_statistics.csv $1/NA_statistics.csv
}

copy_lamda_res()
{
    mkdir $1
    mv NA_cohesive-sequence.fasta $1/NA_cohesive-sequence.fasta
    mv NA_PhageTerm_report.pdf $1/NA_PhageTerm_report.pdf
    mv NA_cohesive-sequence.fasta $1/NA_cohesive-sequence.fasta
    mv NA_statistics.csv $1/NA_statistics.csv
}

check_lamda_res()
{
if [ ! -f NA_cohesive-sequence.fasta ]; then
    echo "NA_cohesive-sequence.fasta does not exist!"
    exit 1
fi
if [ ! -f NA_PhageTerm_report.pdf ]; then
    echo "NA_PhageTerm_report.pdf does not exist!"
    exit 2
fi
if [ ! -f NA_sequence.fasta ]; then
    echo "NA_sequence.fasta does not exist!"
    exit 3
fi
if [ ! -f NA_statistics.csv ]; then
    echo "NA_statistics.csv does not exist!"
    exit 4
fi
}

check_StaphN1_T7_res()
{
if [ ! -f NA_PhageTerm_report.pdf ]; then
    echo "NA_PhageTerm_report.pdf does not exist!"
    exit 8
fi
if [ ! -f NA_sequence.fasta ]; then
    echo "NA_sequence.fasta does not exist!"
    exit 9
fi
if [ ! -f NA_statistics.csv ]; then
    echo "NA_statistics.csv does not exist!"
    exit 10
fi
if [ ! -f NA_direct-term-repeats.fasta ]; then
    echo "NA_direct-term-repeats.fasta does not exist!"
    exit 11
fi
}

copy_StaphN1_T7_res()
{
    mkdir $1
    mv NA_PhageTerm_report.pdf $1/NA_PhageTerm_report.pdf
    mv NA_sequence.fasta $1/NA_sequence.fasta
    mv NA_statistics.csv $1/NA_statistics.csv
    mv NA_direct-term-repeats.fasta $1/NA_direct-term-repeats.fasta
}
