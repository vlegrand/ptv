import csv

from common import check_files, remove_files, K_OK, K_PEAK_WRONG_POS

DATA_PATH="./data/"
REF_RES_PATH="./reference_results/res_T4.20/"
ref_filename=REF_RES_PATH+"NA_sequence.fasta"
new_filename="NA_sequence.fasta"
stats_filename="NA_statistics.csv"
report_filename="NA_PhageTerm_report.pdf"

# 1rst, check that all files exist

ret=check_files(new_filename,stats_filename,report_filename,ref_filename)
if ret !=K_OK:
    exit(ret)
# d not check peak positions. Too many very small peaks.
# expected positions for the 5 most significative + peaks
#expected_pos_pp=(62,168783,168733,135709,244)
expected_pos_pp=(168783,168733,135709,62,168889,159448)
expected_pos_mp=(168852,168826,168784,141471,168779,168703,52897) # There are ex aequos for the 5 biggest T- so I have to consider more peak positions than 5 fot this test. 

reader = csv.reader(open("NA_statistics.csv"), delimiter=",")
# check the 5 most significative peaks in + and - direction.
sortedlist = sorted(reader, key=lambda row: row[3], reverse=True)
for i in range(1,6):
    r=sortedlist[i]
    pos_peak=r[0]
    T=r[3]
    pval=r[5]
    print i,r
#    if (int(pos_peak) not in expected_pos_pp):
#        print "peak + not at the right position: expected ",expected_pos_pp[i-1],"; got: ", pos_peak
#        exit(K_PEAK_WRONG_POS)
    if (T>0.1 and pval<0.05):
        print "Got a peak at position: ",pos_peak," with T>0.1 and pval<0.05"
        exit(K_WRONG_T4_PEAK)

reader = csv.reader(open("NA_statistics.csv"), delimiter=",")
sortedlist = sorted(reader, key=lambda row: row[9], reverse=True)
for i in range(1,6):
    r=sortedlist[i]
    pos_peak=r[0]
    T=r[9]
    pval=r[11]
    print i,r
#    if (int(pos_peak) not in expected_pos_mp):
#        print "peak - not at the right position: expected ",expected_pos_pp[i-1],"; got: ", pos_peak
#        exit(K_PEAK_WRONG_POS)
    if (T>0.1 and pval<0.05):
        print "Got a peak at position: ",pos_peak," with T>0.1 and pval<0.05"
        exit(K_WRONG_T4_PEAK)

# clean files
remove_files(new_filename,stats_filename,report_filename)
