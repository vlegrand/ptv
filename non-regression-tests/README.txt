DESCRIPTION
-----------------------------------------------------------------------------------------------------------------------
These tests aim at checking that refactoring in the code and porting it to python 3 do not induce changes in what is crucial in the final result.

The file virome_assembly_raw.fa is the concatenation of all other .fasta files.

AUTHOR
----------------------------------------------------------------------------------------------------------------------
Véronique Legrand vlegrand@pasteur.fr
Data for the tests were provided by Julian Garneau 
Explanation on what statistics should be carefully looked at was provided by Marc Monot
