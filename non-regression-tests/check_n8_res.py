import os

from common import check_files, remove_files, K_OK,K_WRONG_SEQ,K_WRONG_COH

DATA_PATH="./data/"
REF_RES_PATH="./reference_results/res_n8/"
ref_filename=REF_RES_PATH+"n8_sequence.fasta"
ref_cohesive=REF_RES_PATH+"n8_cohesive-sequence.fasta"
new_filename="n8_sequence.fasta"
stats_filename="n8_statistics.csv"
report_filename="n8_PhageTerm_report.pdf"
new_cohesive="n8_cohesive-sequence.fasta"


ret=check_files(new_filename,stats_filename,report_filename,ref_filename)
if ret !=K_OK:
    exit(ret)

f_ref=open(ref_filename,"r")
ref_s=f_ref.read()
f_ref.close()

f_new=open(new_filename,"r")
new_s=f_new.read()
f_new.close()

if (new_s!=ref_s):
    print("rearranged sequence does not match what is expected")
    exit(K_WRONG_SEQ)

f_ref=open(ref_cohesive,"r")
ref_cs=f_ref.read()
f_ref.close()

f_new=open(new_cohesive,"r")
new_cs=f_new.read()
f_new.close()

if (new_cs!=ref_cs):
    print("cohesive sequence does not match what is expected")
    exit(K_WRONG_COH)

# clean files
remove_files(new_filename,stats_filename,report_filename)
os.remove(new_cohesive)