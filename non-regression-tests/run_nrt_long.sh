#/bin/bash
## VL: decided to write the checking of results in python scripts rather than shel script for portability.
echo "running non regression tests (short version)"
./run_HK97_long.sh || exit 1
./run_Lamda_long.sh || exit 1
./run_StaphN1_long.sh || exit 1
./run_P1_long.sh || exit 1
./run_T4_long.sh || exit 1
./run_T7_long.sh || exit 1
