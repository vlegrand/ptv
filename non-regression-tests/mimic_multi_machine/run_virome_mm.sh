#!/bin/sh
#mimic multi machine mode to make sure we still have the same results.
DATA_PATH=../data
mkdir tmp
for i in 0 1 2 3 4
do
	python ../../phagetermvirome/PhageTerm.py --mm --dir_cov_mm tmp -c 5 --core_id $i -f "$DATA_PATH/R1_1M_READS_EACH_PHAGE(1).fastq.4"  -p "$DATA_PATH/R2_1M_READS_EACH_PHAGE(1).fastq.4" -r "$DATA_PATH/virome_6seq.fa" || exit 1$i
done

mkdir tmp2
mkdir DR

for i in 0 1 2 3 4 5
do
python ../../phagetermvirome/PhageTerm.py --mm --dir_cov_mm=tmp --dir_seq_mm=tmp2 --DR_path=DR --seq_id=$i --nb_pieces=5 -f "$DATA_PATH/R1_1M_READS_EACH_PHAGE(1).fastq.4" -p "$DATA_PATH/R2_1M_READS_EACH_PHAGE(1).fastq.4" -r "$DATA_PATH/virome_6seq.fa" || exit 2$i
done

python ../../phagetermvirome/PhageTerm.py --mm -f "$DATA_PATH/R1_1M_READS_EACH_PHAGE(1).fastq.4" -p "$DATA_PATH/R2_1M_READS_EACH_PHAGE(1).fastq.4" -r "$DATA_PATH/virome_6seq.fa" --DR_path=DR --dir_seq_mm=tmp2 ||exit 3
python check_virome_res.py
