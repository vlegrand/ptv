import os
from filecmp import dircmp
tmp_dir="tmp"
nb_cores=5
nb_seq=6
k_missing_npz_file=1
k_missing_nb_seq_file=2
k_wrong_seq_nbr=3
k_diff_in_dr=4
for c in range(nb_cores):
    for s in range(nb_seq):
        ficname="coverage"+str(s)+"_"+str(c)+".npz"
        if os.path.exists(os.path.join(tmp_dir,ficname))==False:
            print("file ",os.path.join(tmp_dir,ficname)," should exist")
            exit(k_missing_npz_file)
if os.path.exists(os.path.join(tmp_dir,"nb_seq_processed.txt"))==False:
    print("file nb_seq_processed.txt doesn't exist")
    exit(k_missing_nb_seq_file)

f=open(os.path.join(tmp_dir,"nb_seq_processed.txt"))
str_nb_seq=f.read()
f.close()
if int(str_nb_seq)!=nb_seq:
    print("Wrong number of sequences processed")
    exit(k_wrong_seq_nbr)
dcmp=dircmp("DR","DR_ref")
if len(dcmp.diff_files)!=0:
    print("found diffrences in DR diretory content: ",dcmp.diff_files)
    exit(k_diff_in_dr)
exit(0)


