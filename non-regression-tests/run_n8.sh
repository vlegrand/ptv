#/bin/bash
DATA_PATH=./data
SCRIPT_PATH=../phagetermvirome

echo "running PhageTerm to check that the rearanged sequence bug is fixed"
python $SCRIPT_PATH/PhageTerm.py -f "$DATA_PATH/n8_1.fq.gz" -p "$DATA_PATH/n8_1.fq.gz" -r $DATA_PATH/n8.fasta --report_title="n8" --nrt || exit 1
python check_HK97_n8_class.py || exit 2
python check_n8_res.py ||exit 3

