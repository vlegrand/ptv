import csv

from common import check_files, check_peak, remove_files, K_OK

DATA_PATH="./data/"
REF_RES_PATH="./reference_results/res_P1.20/"
ref_filename=REF_RES_PATH+"NA_sequence.fasta"
new_filename="NA_sequence.fasta"
stats_filename="NA_statistics.csv"
report_filename="NA_PhageTerm_report.pdf"

# 1rst, check that all files exist

ret=check_files(new_filename,stats_filename,report_filename,ref_filename)
if ret !=K_OK:
    exit(ret)


reader = csv.reader(open("NA_statistics.csv"), delimiter=",")
sortedlist = sorted(reader, key=lambda row: row[3], reverse=True)
## There are often 2 ex aequo results (when sorting on T+ in veverse order): first peak is 174 but its pvalue is 1 so it is not relevant. check the other ones.
# extract all ex aequo or bigger T (and check that because of their pvalue they are not relevant). Up to the 5 biggest T should be OK.
nb=5
finalists=sortedlist[1:nb]
for i in range(0,nb):
    r=finalists[i]
    pos_peak=r[0]
    T=r[3]
    pval=r[5]
    if float(pval)<1: # 1rst peak encountered with a pvalue smaller than 1 should be the right one
        break
print(r)

# Only + peak is of importance for this phage.
ret=check_peak(54885,pos_peak,T,pval)
if ret!=K_OK:
    exit(ret)


# clean files
remove_files(new_filename,stats_filename,report_filename)
