#/bin/bash
## VL: decided to write the checking of results in python scripts rather than shel script for portability.
echo "running non regression tests (parallel version; requires 4 cores)"
./run_HK97_long_multiproc.sh || exit 1
./run_Lamda_long_multiproc.sh || exit 1
./run_StaphN1_long_multiproc.sh || exit 1
./run_P1_long_multiproc.sh || exit 1
./run_T4_long_multiproc.sh || exit 1
./run_T7_long_multiproc.sh || exit 1
