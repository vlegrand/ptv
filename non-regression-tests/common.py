import os
import filecmp

K_PEAK_WRONG_POS=2
K_WRONG_T=3
K_WRONG_P=4
K_WRONG_T4_PEAK=5

K_FASTA_DOESNT_EXIST=8
K_CSV_DOESNT_EXIST=9
K_PDF_DOESNT_EXIST=10
K_FASTA_DIFFERS=11
K_REPEATS_DOESNT_EXIST=12

K_CLASSFIC_DOESNT_EXIST=20
K_WRONG_CLASS=21

K_WRONG_SEQ=30
K_WRONG_COH=31

K_OK=0


def check_peak(expected_pos,pos_peak,T,pval):
    if (int(pos_peak) != expected_pos):
        print(("peak + not at the right position: expected ",expected_pos,"; got: ", pos_peak))
        return K_PEAK_WRONG_POS
    if (float(T) <= 0.1):
        print(("wrong T value: ", T))
        return K_WRONG_T
    if (float(pval) >= 0.05):
        print(("wrong pval value for + peak: ", pval))
        return K_WRONG_P
    return K_OK



def check_files(seq_filename,stats_filename,report_filename,ref_filename):
    # 1rst, check that all files exist
    if not (os.path.exists(seq_filename)):
        print(seq_filename, " not found")
        return K_FASTA_DOESNT_EXIST

    if not (os.path.exists(stats_filename)):
        print(stats_filename, " not found")
        return K_CSV_DOESNT_EXIST

    if not (os.path.exists(report_filename)):
        print(report_filename, " not found")
        return K_PDF_DOESNT_EXIST

    if (filecmp.cmp(ref_filename, seq_filename) == False):
        print(ref_filename, " and " + seq_filename + " differ!")
        return K_FASTA_DIFFERS
    return K_OK

def check_files_repeat(seq_filename,stats_filename,report_filename,repeats_filename,ref_filename):
    check_files(seq_filename, stats_filename, report_filename, ref_filename)
    if not (os.path.exists(repeats_filename)):
        print(repeats_filename, " not found")
        return K_REPEATS_DOESNT_EXIST
    return K_OK


def remove_files(seq_filename,stats_filename,report_filename):
    os.remove(seq_filename)
    os.remove(report_filename)
    os.remove(stats_filename)

def remove_files_repeat(seq_filename,stats_filename,report_filename,repeats_filename):
    remove_files(seq_filename,stats_filename,report_filename)
    os.remove(repeats_filename)

def check_class(filename,expected_class):
    if not (os.path.exists(filename)):
        return K_CLASSFIC_DOESNT_EXIST
    else:
        f=open(filename,"r")
        cl=f.read()
        if (cl!=expected_class):
            print("found ",cl," instead of: ",expected_class)
            return K_WRONG_CLASS
        f.close()
    os.remove(filename)
    return K_OK


