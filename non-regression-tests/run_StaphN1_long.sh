#/bin/bash
DATA_PATH=./data
SCRIPT_PATH=../phagetermvirome

echo "running PhageTerm on Staph1N genome and a dataset with a minimum 50x coverage"
python $SCRIPT_PATH/PhageTerm.py --nrt -f "$DATA_PATH/R1_1M_READS_EACH_PHAGE(1).fastq.4" -p "$DATA_PATH/R2_1M_READS_EACH_PHAGE(1).fastq.4" -r $DATA_PATH/Staph1N_assembly.fasta || exit 1
python check_Staph1N_class.py || exit 2
