##
# This script is used to select 1 read out of N from input file.
# param 1 is N
# param 2 is input filename
# output filename is input_filename.N
import sys
fastq=sys.argv[2]
N=sys.argv[1]
output_filename=fastq+"."+N
fileout=open(output_filename,"w+")
N=int(N)
filin = open(fastq)
line = filin.readline()
cnt_read=0
select_read=False
num_line=0
str_read=""
while line:
    # Read sequence
    read = line.split("\n")[0].split("\r")[0]
    if (read[0] == '@' and num_line % 4 == 0):  # make sure we don't take into account a quality score instead of a read.
        cnt_read+=1
        if cnt_read % N == 0 : # keep this read.
            select_read=True
        elif str_read!="":
            # flush read to output file
            fileout.write(str_read)
            select_read=False
            str_read=""
    if select_read==True:
        str_read+=line
    line = filin.readline()
if select_read==True:
    fileout.write(str_read)
filin.close()
fileout.close()
