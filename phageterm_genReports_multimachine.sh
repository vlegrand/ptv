#!/bin/sh
## This script must not be ran until all jobs in the step 2 job array have completed.
cd DIR_WHERE_YOU_CLONED_THE_SOURCES
echo "python PhageTerm.py -f INPUT_FASTQ_FILE -p INPUT_PE_FILE -r REFERENCE_FILE --DR_path=PATH_TO_DR_CONTENT  --mm --dir_seq_mm=pasteur/scratch/users/vlegrand/seq_res_586_cpu"
python PhageTerm.py  -f INPUT_FASTQ_FILE -p INPUT_PE_FILE -r REFERENCE_FILE --DR_path=PATH_TO_DR_CONTENT --mm --dir_seq_mm=/pasteur/scratch/users/vlegrand/seq_res_586_cpu
